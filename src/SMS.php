<?php
/**
 * Created by PhpStorm.
 * User: taufik
 * Date: 16/10/18
 * Time: 15:56
 */

namespace taufikterdidik\tcastsms;


use GuzzleHttp\Client;

class SMS
{
    private $base_uri = 'http://ip:20003/';
    private $account = '***';
    private $password = '***';
    private $message;
    private $numbers = [];
    private $msg_status = [];
    private $balance = null;
    private $gift = null;

    function __construct($config = [])
    {
        foreach ($config as $key => $value){
            $this->$key = $value;
        }
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function addNumber($number)
    {
        $this->numbers[] = $this->properNumbers($number);
    }

    public function queryBalance()
    {
        $client = new Client(['base_uri' => $this->base_uri]);
        $res = $client->request('GET','getbalance',
            ['query'=>['account'=>$this->account, 'password'=>$this->password]]);
        $data = json_decode($res->getBody(),true);
        return $data;
    }

    public function getBalance()
    {
        $data = $this->queryBalance();
        $this->balance = intval($data['balance']);
        $this->gift = intval($data['gift']);
        return $this->balance;
    }

    private function properNumbers($number){
        $tmp = preg_replace("/[^0-9]/","",$number);
        if(substr($number,0,1)=='0'){
            $tmp = '62'.substr($number,1);
        }
        return $tmp;
    }

    public function send()
    {
        $client = new Client(['base_uri' => $this->base_uri]);
        $res = $client->request('GET','sendsms',
            ['query'=>[
                'account'=>$this->account, 'password'=>$this->password,
                'numbers'=>implode(',',$this->numbers),
                'content'=>$this->message]]);
        $data = json_decode($res->getBody(),true);
        $this->msg_status = $data['array'];
        return $data;
    }
}