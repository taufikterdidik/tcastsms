<?php
/**
 * Created by PhpStorm.
 * User: taufik
 * Date: 17/10/18
 * Time: 9:09
 */

final class SMSTest extends \PHPUnit\Framework\TestCase
{
    private $config = ['base_uri' => 'http://ip:20003/',
        'account' => '***',
        'password' => '***'];

    public function testSuksesQueryBalance(){
        $sms = new \taufikterdidik\tcastsms\SMS($this->config);
        $balance = $sms->queryBalance();
        $this->assertArrayHasKey('status',$balance);
        $this->assertArrayHasKey('balance',$balance);
        $this->assertArrayHasKey('gift',$balance);
    }

    public function testSuksesGetBalance(){
        $sms = new \taufikterdidik\tcastsms\SMS($this->config);
        $this->assertInternalType('int',$sms->getBalance());
    }

    public function testKirimSMS(){
        $sms = new \taufikterdidik\tcastsms\SMS($this->config);
        $sms->setMessage('Test Kirim SMS');
        $sms->addNumber('08123456789');
        $result = $sms->send();
        $this->assertArrayHasKey('status', $result);
        $this->assertArrayHasKey('array', $result);
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('fail', $result);
    }
}