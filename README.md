# API Gateway untuk TCASTSMS

## Penggunaan

```
<?php
  require __DIR__.'/vendor/autoload.php';
  $config = [
    'base_uri' => 'http://ipserver:20003/',
    'account' => '***',
    'password' => '***'
  ];
  $sms = new \taufikterdidik\tcastsms\SMS($config);
  echo 'Saldo: '.$sms->getBalance();
  
  $sms->setMessage('Test Kirim SMS');
  $sms->addNumber('08123456789');
  if($sms->send()){
    echo 'sukses kirim sms';
  } else {
    echo 'gagal kirim sms';
  }
```